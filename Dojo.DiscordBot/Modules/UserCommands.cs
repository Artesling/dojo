﻿using Discord.Interactions;
using Dojo.API.Services;

namespace Dojo.DiscordBot.Modules;

[Group("user", "Пользовательские команды")]
public class UserCommands : InteractionModuleBase<SocketInteractionContext>
{
    public readonly DiscordService _discordService;

    public UserCommands(DiscordService discordService)
    {
        _discordService = discordService;
    }
    
    [SlashCommand("register", "Зарегистрироваться")]
    public async Task Register(string username)
    {
        try
        {
            await _discordService.RegisterUserAsync(Context.User.Id, username, Context.Guild.Id);
        }
        catch (Exception e)
        {
            await RespondAsync(e.Message);
        }

        await RespondAsync("krasava man4ik");
    }
}