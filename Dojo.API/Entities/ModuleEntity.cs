﻿using Dojo.API.Enums;

namespace Dojo.API.Entities;
//TODO: make modules related to CafeEntity
public class ModuleEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
    public ModuleState State { get; set; }
}