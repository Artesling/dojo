﻿namespace Dojo.API.Controllers;

public class PrizeTypeEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
}