﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Dojo.API.Entities;

public class UserGambleInfoEntity
{
    public UserGambleInfoEntity()
    {
        RouletteChance = 50;
        LastUseRoulette = new DateTime(0);
    }

    public long Id { get; set; }
    public long UserId { get; set; }
    public DateTime LastUseRoulette { get; set; }
    public double RouletteChance { get; set; }
    
    [ForeignKey(nameof(UserId))]
    public virtual UserEntity User { get; set; }
}