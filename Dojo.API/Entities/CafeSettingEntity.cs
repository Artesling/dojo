﻿namespace Dojo.API.Entities;

public class CafeSettingEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }
}