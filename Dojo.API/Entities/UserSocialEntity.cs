﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Dojo.API.Entities;

public class UserSocialEntity
{
    public long Id { get; set; }
    public long UserId { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public ulong? DiscordId { get; set; }
    public long? TelegramId { get; set; }
    public long? VkontakteId { get; set; }
    
    [ForeignKey(nameof(UserId))]
    public virtual UserEntity User { get; set; }
}