﻿using Dojo.API.Controllers;

namespace Dojo.API.Entities;

public class PrizeEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long Quantity { get; set; }
    public double Chance { get; set; }

    public virtual PrizeTypeEntity Type { get; set; }
}