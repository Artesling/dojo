﻿using System.Collections.ObjectModel;
using Dojo.API.CRM;
using Dojo.API.Enums;

namespace Dojo.API.Entities;

public class CafeEntity
{
    public long Id { get; set; }
    public required string Name { get; set; }
    public required string Address { get; set; }
    public required CRMType CRMType { get; set; }
    public virtual CafeSocialEntity Social { get; set; } = new();
    public virtual CafeCRMCredentialEntity CRMCredential { get; set; } = new();
    public virtual List<CafeSettingEntity> CafeSettings { get; set; } = new()
    {
        new CafeSettingEntity
        {
            Name = "Next gamble use in minutes",
            Value = "1440"
        },
        new CafeSettingEntity
        {
            Name = "Discord server id",
            Value = ""
        }
    };
    public virtual ObservableCollection<UserEntity> Users { get; set; }
}