﻿namespace Dojo.API.Entities;

public class GlobalSettingEntity
{
    public long Id { get; set; }
    public required string Name { get; set; }
    public required string Value { get; set; }
}