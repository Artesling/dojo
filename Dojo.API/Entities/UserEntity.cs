﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.Entities;
public class UserEntity
{
    public long Id { get; set; }
    public required string Username { get; set; }
    public string? Password { get; set; }
    public long CafeId { get; set; }
    public long RoleId { get; set; }
    
    public virtual UserGambleInfoEntity GambleInfo { get; set; } = new();
    public virtual UserSocialEntity Social { get; set; } = new();
    [ForeignKey(nameof(RoleId))]
    public virtual RoleEntity Role { get; set; }
    [ForeignKey(nameof(CafeId))]
    public virtual CafeEntity Cafe { get; set; }
    //TODO: implement inventoryentity
}