﻿using System.Collections.ObjectModel;

namespace Dojo.API.Entities;

public class RoleEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
    public virtual ObservableCollection<UserEntity> Users { get; set; }
}