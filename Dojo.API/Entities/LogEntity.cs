﻿using System.ComponentModel.DataAnnotations.Schema;
using Dojo.API.Enums;

namespace Dojo.API.Entities;

public class LogEntity
{
    public long Id { get; set; }
    public LogType Type { get; set; }
    public DateTime Date { get; set; }
    public string Message { get; set; }

    public virtual ModuleEntity Module { get; set; }
    public virtual UserEntity User { get; set; }
}