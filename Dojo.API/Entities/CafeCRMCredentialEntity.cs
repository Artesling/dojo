﻿using Dojo.API.Enums;

namespace Dojo.API.Entities;

public class CafeCRMCredentialEntity
{
    public long Id { get; set; }
    public long? LicenseId { get; set; }
    public long? CafeId { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }
    public string? AuthToken { get; set; }
}