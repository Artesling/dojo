﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class UserProfile : Profile
{
    public UserProfile()
    {
        //User
        CreateMap<UserEntity, UserModel>();
        CreateMap<UserModel, UserEntity>();
        
        //User social
        CreateMap<UserSocialEntity, UserSocialModel>();
        CreateMap<UserSocialModel, UserSocialEntity>();
        
        //User gamble info
        CreateMap<UserGambleInfoEntity, UserGambleInfoModel>();
        CreateMap<UserGambleInfoModel, UserGambleInfoEntity>();
    }
}