﻿using AutoMapper;
using Dojo.API.Controllers;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class PrizeProfile : Profile
{
    public PrizeProfile()
    {
        //Prize
        CreateMap<PrizeEntity, PrizeModel>();
        CreateMap<PrizeModel, PrizeEntity>();
        
        //Prize type
        CreateMap<PrizeTypeEntity, PrizeTypeModel>();
        CreateMap<PrizeTypeModel, PrizeTypeEntity>();
    }
}