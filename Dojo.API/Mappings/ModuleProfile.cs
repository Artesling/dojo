﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class ModuleProfile : Profile
{
    public ModuleProfile()
    {
        CreateMap<ModuleEntity, ModuleModel>();
        CreateMap<ModuleModel, ModuleEntity>();
    }
}