﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class CafeProfile : Profile
{
    public CafeProfile()
    {
        // Cafe
        CreateMap<CafeEntity, CafeModel>()
            .ForMember(m => m.UsersCount, x => x.MapFrom(e => e.Users.Count));
        CreateMap<CafeModel, CafeEntity>();
        
        //CafeSocial
        CreateMap<CafeSocialEntity, CafeSocialModel>();
        CreateMap<CafeSocialModel, CafeSocialEntity>();
        
        //CafeSettings
        CreateMap<CafeSettingEntity, CafeSettingModel>();
        CreateMap<CafeSettingModel, CafeSettingEntity>();
    }
}