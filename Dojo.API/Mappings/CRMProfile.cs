﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class CRMProfile : Profile
{
    public CRMProfile()
    {
        CreateMap<CafeCRMCredentialEntity, CRMCredentialModel>();
        CreateMap<CRMCredentialModel, CafeCRMCredentialEntity>();
    }
}