﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class LogProfile : Profile
{
    public LogProfile()
    {
        CreateMap<LogEntity, LogModel>();
        CreateMap<LogModel, LogEntity>();
    }
}