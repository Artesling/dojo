﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Mappings;

public class RoleProfile : Profile
{
    public RoleProfile()
    {
        CreateMap<RoleEntity, RoleModel>();
        CreateMap<RoleModel, RoleEntity>();
    }
}