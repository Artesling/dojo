﻿using Dojo.API.Enums;

namespace Dojo.API.Models;

public class LogModel
{
    public long Id { get; set; }
    public LogType Type { get; set; }
    public DateTime Date { get; set; }
    public string Message { get; set; }
    //public ServiceModel Service { get; set;}
    public UserModel User { get; set; }
}