﻿namespace Dojo.API.Models;

public class CRMCredentialModel
{
    public long Id { get; set; }
    public long? LicenseId { get; set; }
    public long? CafeId { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }
    public string? AuthToken { get; set; }
}