﻿namespace Dojo.API.Models;

public class UserGambleInfoModel
{
    public long Id { get; set; }
    public DateTime LastUseRoulette { get; set; }
    public double RouletteChance { get; set; }
}