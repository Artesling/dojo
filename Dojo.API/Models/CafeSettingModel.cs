﻿namespace Dojo.API.Models;

public class CafeSettingModel
{
    public string Name { get; set; }
    public string Value { get; set; }
}