﻿namespace Dojo.API.Models;

public class CafeSocialModel
{
    public long Id { get; set; }
    public string? Website { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public ulong? DiscordServerId { get; set; }
    public long? TelegramChatId { get; set; }
}