﻿using Dojo.API.Enums;

namespace Dojo.API.Models;

public class CafeModel
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public CRMType CRMType { get; set; }
    public CafeSocialModel Social { get; set; }
    public CRMCredentialModel CRMCredential { get; set; }
    public List<CafeSettingModel> CafeSettings { get; set; }
    public long UsersCount { get; set; }
}