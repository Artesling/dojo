﻿namespace Dojo.API.Models;

public class RoleModel
{
    public long Id { get; set; }
    public string Name { get; set; }
}