﻿namespace Dojo.API.Models;

public class PrizeTypeModel
{
    public long Id { get; set; }
    public string Name { get; set; }
}