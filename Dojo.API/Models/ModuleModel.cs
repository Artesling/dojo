﻿using Dojo.API.Enums;

namespace Dojo.API.Models;

public class ModuleModel
{
    public long Id { get; set; }
    public string Name { get; set; }
    public ModuleState State { get; set; }
}