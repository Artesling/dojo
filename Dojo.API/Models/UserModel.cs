﻿namespace Dojo.API.Models;

public class UserModel
{
    public long Id { get; set; }
    public string Username { get; set; }
    public string? Password { get; set; }
    public long CafeId { get; set; }

    public UserSocialModel Social { get; set; }
    public UserGambleInfoModel GambleInfo { get; set; }
    public RoleModel Role { get; set; }
}