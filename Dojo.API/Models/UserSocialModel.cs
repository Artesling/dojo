﻿namespace Dojo.API.Models;

public class UserSocialModel
{
    public long Id { get; set; }
    public string? Phone { get; set; }
    public string? Email { get; set; }
    public ulong? DiscordId { get; set; }
    public long? TelegramId { get; set; }
    public long? VkontakteId { get; set; }
}