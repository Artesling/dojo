﻿namespace Dojo.API.Models;

public class PrizeModel
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long Quantity { get; set; }
    public double Chance { get; set; }
    public string Type { get; set; }
}