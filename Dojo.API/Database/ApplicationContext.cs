﻿using Dojo.API.Controllers;
using Dojo.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API
{
    public class ApplicationContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<UserSocialEntity> UserSocials { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        public DbSet<LogEntity> Logs { get; set; }
        public DbSet<ModuleEntity> Modules { get; set; }
        public DbSet<PrizeEntity> Prizes { get; set; }
        public DbSet<PrizeTypeEntity> PrizeTypes { get; set; }
        public DbSet<UserGambleInfoEntity> UserGambleInfo { get; set; }
        public DbSet<CafeEntity> Cafes { get; set; }
        public DbSet<CafeCRMCredentialEntity> CafeCRMCredentials { get; set; }
        public DbSet<CafeSocialEntity> CafeSocials { get; set; }
        public DbSet<CafeSettingEntity> CafeSettings { get; set; }
        public DbSet<GlobalSettingEntity> GlobalSettings { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=Dojo;Trusted_Connection=True;TrustServerCertificate=True;");
        }
    }
}
