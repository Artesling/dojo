﻿namespace Dojo.API;
//TODO: implement updateorinsertrange, delete, deleterange (not used yet but gonna be in use)
public class Database
{
    private static readonly ApplicationContext _dataBase = new ApplicationContext();

    public static Microsoft.EntityFrameworkCore.DbSet<T> Get<T>() where T : class
    {
        return _dataBase.Set<T>();
    }

    public static int UpdateOrInsert<T>(T record) where T : class
    {
        var dbSet = Get<T>();

        if (dbSet.Any(x => x.Equals(record)))
        {
            dbSet.Update(record);
        }
        else
        {
            dbSet.Add(record);
        }

        return _dataBase.SaveChanges();
    }

    public static Task<int> UpdateOrInsertAsync<T>(T record) where T : class
    { 
        return Task.FromResult<int>(UpdateOrInsert(record));
    }
}