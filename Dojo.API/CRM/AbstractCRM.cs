﻿using Dojo.API.Entities;
using Dojo.API.Enums;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.CRM;
public abstract class AbstractCRM
{
    private static List<AbstractCRM> CRMs = new List<AbstractCRM>()
    {
        new ICafe(),
    };

    public static AbstractCRM GetCRMByType(CRMType crmType)
    {
        return CRMs.FirstOrDefault(x => x.GetType().Name == crmType.ToString());
    }

    public abstract Task<double> GetUserMoney(CafeCRMCredentialEntity creds, string username);
    public abstract Task<double> GetUserBonus(CafeCRMCredentialEntity creds, string username);
    public abstract Task<double> AddMoneyToUser(CafeCRMCredentialEntity creds, string username, double sum);
    public abstract Task<double> AddBonusToUser(CafeCRMCredentialEntity creds, string username, double sum);
    public abstract Task<double> SetUserMoney(CafeCRMCredentialEntity creds, string username, double sum);
    public abstract Task<double> SetUserBonus(CafeCRMCredentialEntity creds, string username, double sum);
    public abstract void UpdateAuthorizationToken(string token);
}