﻿using System.Net.Http.Headers;
using System.Text.Json.Serialization;
using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Exceptions.System;
using Dojo.API.Exceptions.User;
using Newtonsoft.Json;

namespace Dojo.API.CRM;

public class ICafe : AbstractCRM
{
    private HttpClient _client;

    public ICafe()
    {
        _client = new HttpClient();
    }
    
    public override async Task<double> GetUserMoney(CafeCRMCredentialEntity creds, string username)
    {
        UpdateAuthorizationToken(creds.AuthToken);
        
        var user = await _getUserByExactUsername(creds.CafeId.Value, username);

        return double.Parse(user.member_balance.Value);
    }
    public override async Task<double> GetUserBonus(CafeCRMCredentialEntity creds, string username)
    {
        UpdateAuthorizationToken(creds.AuthToken);
        
        var user = await _getUserByExactUsername(creds.CafeId.Value, username);

        return double.Parse(user.member_balance_bonus.Value);
    }
    public override async Task<double> AddMoneyToUser(CafeCRMCredentialEntity creds, string username, double sum)
    {
        UpdateAuthorizationToken(creds.AuthToken);
        
        var userId = await _getUserId(creds.CafeId.Value, username);

        var body = _getBodyForTopUp(userId, sum, 0);
        
        await _client.PostAsync(_getUserTopUpLink(creds.CafeId.Value), body);

        return await GetUserMoney(creds, username);
    }
    public override async Task<double> AddBonusToUser(CafeCRMCredentialEntity creds, string username, double sum)
    {
        UpdateAuthorizationToken(creds.AuthToken);
        
        var userId = await _getUserId(creds.CafeId.Value, username);

        var body = _getBodyForTopUp(userId, 0, sum);
        
        await _client.PostAsync(_getUserTopUpLink(creds.CafeId.Value), body);

        return await GetUserBonus(creds, username);
    }
    public override async Task<double> SetUserMoney(CafeCRMCredentialEntity creds, string username, double sum)
    {
        throw new CannotBeImplemented(nameof(SetUserMoney));
    }
    public override async Task<double> SetUserBonus(CafeCRMCredentialEntity creds, string username, double sum)
    {
        throw new CannotBeImplemented(nameof(SetUserBonus));
    }
    
    public override void UpdateAuthorizationToken(string token)
    {
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    }

    #region Private methods

    private string _getUserSearchLink(long cafeId, string username) => 
        "https://eu3.icafecloud.com/api/v2/cafe/" + cafeId + "/members?sort_name=member_id&sort=desc&search_text=" + username;

    private string _getUserTopUpLink(long cafeId) =>
        "https://eu3.icafecloud.com/api/v2/cafe/" + cafeId + "/members/action/topup";
    private async Task<dynamic> _getUsersByUsername(long cafeId, string username)
    {
        var response = await _client.GetStringAsync(_getUserSearchLink(cafeId, username));
        dynamic result = JsonConvert.DeserializeObject(response);
        return result.data.members;
    }
    private async Task<dynamic> _getUserByExactUsername(long cafeId, string username)
    {
        var members = await _getUsersByUsername(cafeId, username);
        
        if (members.Count > 0)
        {
            for (int i = 0; i < members.Count; i++)
            {
                if (members[i].member_account == username) return members[i];
            }
        }
        
        throw new UserNotFoundException(username);
    }
    private async Task<long> _getUserId(long cafeId, string username)
    {
        var user= await _getUserByExactUsername(cafeId, username);
        return long.Parse(user.member_id.ToString());
    }

    private FormUrlEncodedContent _getBodyForTopUp(double userId, double money, double bonus)
    {
        return new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("topup_ids", userId.ToString()),
            new KeyValuePair<string, string>("action", "ajax_topup_member"),
            new KeyValuePair<string, string>("payment_method_credit_card", "on"),
            new KeyValuePair<string, string>("topup_value", money.ToString()),
            new KeyValuePair<string, string>("topup_value_total", money.ToString()),
            new KeyValuePair<string, string>("topup_balance_bonus", bonus.ToString()),
            new KeyValuePair<string, string>("add_point", "0"),
            new KeyValuePair<string, string>("comment", "")
        });
    }

    #endregion
}