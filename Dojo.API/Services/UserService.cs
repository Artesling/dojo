﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Exceptions.User;
using Dojo.API.Interfaces.Cafe;
using Dojo.API.Interfaces.GlobalSetting;
using Dojo.API.Interfaces.Prize;
using Dojo.API.Interfaces.User;
using Dojo.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.Services;

public class UserService : IUserService
{
    private readonly IMapper _mapper;
    private readonly IGambleService _gambleService;
    private readonly ICafeService _cafeService;
    private readonly IGlobalSettingService _globalSettingService;
    public UserService(IMapper mapper, IGambleService gambleService, ICafeService cafeService, IGlobalSettingService globalSettingService)
    {
        _mapper = mapper;
        _gambleService = gambleService;
        _cafeService = cafeService;
        _globalSettingService = globalSettingService;
    }

    public async Task<List<UserModel>> GetAllUsersAsync()
    {
        var users = await Database.Get<UserEntity>().ToListAsync();
        var userList = new List<UserModel>();

        foreach (var user in users)
        {
            userList.Add(_mapper.Map<UserModel>(user));
        }

        return userList;
    }

    public async Task<UserModel> AddRoleToUserAsync(string username, string roleName)
    {
        var user = await Database.Get<UserEntity>()
            .AsQueryable()
            .Include(x => x.Role)
            .FirstOrDefaultAsync(x => x.Username == username);

        var role = await Database.Get<RoleEntity>()
            .AsQueryable()
            .FirstOrDefaultAsync(x => x.Name == roleName);

        user.Role = role;

        await Database.UpdateOrInsertAsync(user);
        
        return _mapper.Map<UserModel>(user);
    }

    public async Task<UserEntity> CreateUserAsync(UserModel user)
    {
        var userEntity = _mapper.Map<UserEntity>(user);
        userEntity.Cafe = await Database.Get<CafeEntity>().FirstOrDefaultAsync(x => x.Id == user.CafeId);
        userEntity.Role = await Database.Get<RoleEntity>().FirstOrDefaultAsync(x => x.Id == _globalSettingService.GetDefaultRoleIdSync());
        userEntity.GambleInfo = new UserGambleInfoEntity();

        await Database.UpdateOrInsertAsync(userEntity);

        return userEntity;
    }

    public RoleEntity GetUserRole(string username)
    {
        var user = Database.Get<UserEntity>()
            .AsQueryable()
            .Include(x => x.Role)
            .FirstOrDefault(x => x.Username == username);

        return user.Role;
    }
    public async Task<UserEntity?> GetUserByNameAndCafeIdAsync(string username, long cafeId)
    {
        var user = await Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Role)
            .Include(x => x.Social)
            .Include(x => x.GambleInfo)
            .FirstOrDefaultAsync(x => x.Username == username && x.CafeId == cafeId);
        
        return user;
    }

    public async Task<PrizeModel> UseGambleAsync(UserEntity user)
    {
        var prize = await _gambleService.GetRandomPrizeForUserAsync(user.Id);

        user.GambleInfo.LastUseRoulette = DateTime.Now;
        await Database.UpdateOrInsertAsync(user.GambleInfo);

        return _mapper.Map<PrizeModel>(prize);
    }

    public async Task<long> GetMinutesToNextGamble(UserEntity user)
    {
        var nextGambleIn = await _cafeService.GetNextGambleSettingByCafeIdAsync(user.CafeId);

        var nextGambleDate = user.GambleInfo.LastUseRoulette + TimeSpan.FromMinutes(nextGambleIn);
        var nextGambleInMinutes = (long) TimeSpan.FromTicks(nextGambleDate.Ticks - DateTime.Now.Ticks).TotalMinutes;

        return nextGambleInMinutes;
    }
    
    public async Task<UserEntity?> GetUserByDiscordIdAndServerIdAsync(ulong discordUserId, ulong discordServerId)
    {
        var cafe = await _cafeService.GetCafeByDiscordIdAsync(discordServerId);
        
        return await Database.Get<UserEntity>()
            .Include(x => x.Social)
            .Include(x => x.GambleInfo)
            .FirstOrDefaultAsync(x => x.Social.DiscordId == discordUserId && x.CafeId == cafe.Id);
    }
    
    public async Task<UserEntity?> GetUserByDiscordIdAndCafeIdAsync(ulong discordUserId, long cafeId)
    {
        return await Database.Get<UserEntity>()
            .Include(x => x.Social)
            .Include(x => x.GambleInfo)
            .FirstOrDefaultAsync(x => x.Social.DiscordId == discordUserId && x.CafeId == cafeId);
    }

    public async Task<UserEntity> UpdateUserSocialAsync(UserSocialModel social, long userId)
    {
        var socialEntity = _mapper.Map<UserSocialEntity>(social);
        var user = await GetUserByIdAsync(userId);
        user.Social = socialEntity;

        await Database.UpdateOrInsertAsync(user);

        return user;
    }

    public async Task<UserEntity> GetUserByIdAsync(long userId)
    {
        return await Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Social)
            .Include(x => x.GambleInfo)
            .FirstOrDefaultAsync(x => x.Id == userId);
    }
}