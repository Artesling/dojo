﻿using AutoMapper;
using Dojo.API.Exceptions.User;
using Dojo.API.Interfaces.Cafe;
using Dojo.API.Interfaces.Discord;
using Dojo.API.Interfaces.GlobalSetting;
using Dojo.API.Interfaces.User;
using Dojo.API.Models;

namespace Dojo.API.Services;

public class DiscordService : IDiscordService
{
    private readonly IGlobalSettingService _globalSetting;
    private readonly IUserService _userService;
    private readonly ICafeService _cafeService;
    private readonly IMapper _mapper;

    public DiscordService(IGlobalSettingService globalSetting, IUserService userService, ICafeService cafeService, IMapper mapper)
    {
        _globalSetting = globalSetting;
        _userService = userService;
        _cafeService = cafeService;
        _mapper = mapper;
    }

    public async Task<string> GetDiscordDevBotToken()
    {
        return await _globalSetting.GetDiscordDevBotTokenAsync();
    }

    public async Task<string> GetDiscordProdBotToken()
    {
        return await _globalSetting.GetDiscordProdBotTokenAsync();
    }

    public async Task<bool> RegisterUserAsync(ulong discordUserId, string username, ulong discordServerId)
    {
        var userAttachedToDiscord = await _userService.GetUserByDiscordIdAndServerIdAsync(discordUserId, discordServerId);
        
        if (userAttachedToDiscord == null)
        {
            //TODO: log discord register
            var cafe = await _cafeService.GetCafeByDiscordIdAsync(discordServerId);

            var userAttachedToCafe = await _userService.GetUserByNameAndCafeIdAsync(username, cafe.Id);

            if (userAttachedToCafe == null)
            {
                var userModel = new UserModel()
                {
                    Username = username,
                    CafeId = cafe.Id,
                    Social = new UserSocialModel()
                    {
                        DiscordId = discordUserId
                    }
                };

                await _userService.CreateUserAsync(userModel);
            }
            else
            {
                userAttachedToCafe.Social.DiscordId = discordUserId;

                var social = _mapper.Map<UserSocialModel>(userAttachedToCafe.Social);
                
                await _userService.UpdateUserSocialAsync(social, userAttachedToCafe.Id);
            }

            return true;
        }
        throw new UserAlreadyExistsException(discordUserId);
    }
}