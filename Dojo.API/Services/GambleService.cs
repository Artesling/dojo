﻿using AutoMapper;
using Dojo.API.Controllers;
using Dojo.API.Entities;
using Dojo.API.Interfaces.Prize;
using Dojo.API.Interfaces.User;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.Services;

public class GambleService : IGambleService
{
    private readonly IMapper _mapper;

    public GambleService(IMapper mapper)
    {
        _mapper = mapper;
    }

    public async Task<List<PrizeEntity>> GetAllPrizesAsync()
    {
        return await Database.Get<PrizeEntity>().ToListAsync();
    }

    public async Task<PrizeEntity> CreatePrizeAsync(string name, long quantity, double chance, PrizeTypeEntity type)
    {
        var prize = new PrizeEntity
        {
            Name = name,
            Quantity = quantity,
            Chance = chance,
            Type = type
        };

        await Database.UpdateOrInsertAsync(prize);

        return prize;
    }
    public async Task<UserGambleInfoEntity> GetUserGambleInfoEntityByIdAsync(long userId)
    {
        return await Database.Get<UserGambleInfoEntity>()
            .FirstOrDefaultAsync(x => x.Id == userId);
    }

    public async Task<PrizeEntity> GetRandomPrizeForUserAsync(long userId)
    {
        var prizes = await GetAllPrizesAsync();

        var userGambleInfo = await GetUserGambleInfoEntityByIdAsync(userId);

        var firstRandomRound = new Random().NextDouble() * 100;

        PrizeEntity winner = prizes.First();
        double currentChance = 0;

        foreach (var prize in prizes)
        {
            currentChance += prize.Chance;
            if (currentChance >= firstRandomRound)
            {
                winner = prize;
                break;
            }
        }
        
        var secondRandomRound = new Random().NextDouble() * 100;

        if (userGambleInfo.RouletteChance <= secondRandomRound)
        {
            return winner;
        }

        return null;
    }

    public async Task<List<PrizeTypeEntity>> GetAllPrizeTypesAsync()
    {
        return await Database.Get<PrizeTypeEntity>().ToListAsync();
    }
    
    public async Task<PrizeTypeEntity> CreatePrizeTypeAsync(string name)
    {
        var prizeType = new PrizeTypeEntity
        {
            Name = name
        };

        await Database.UpdateOrInsertAsync(prizeType);

        return prizeType;
    }

    public async Task<PrizeTypeEntity> GetPrizeTypeByIdAsync(long id)
    {
        return await Database.Get<PrizeTypeEntity>().FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<PrizeTypeEntity> GetPrizeTypeByNameAsync(string name)
    {
        return await Database.Get<PrizeTypeEntity>().FirstOrDefaultAsync(x => x.Name == name);
    }
}