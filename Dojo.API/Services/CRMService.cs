﻿using Dojo.API.CRM;
using Dojo.API.Entities;
using Dojo.API.Interfaces.CRM;
using Dojo.API.Interfaces.User;

namespace Dojo.API.Services;

public class CRMService : ICRMService
{
    private readonly IUserService _userService;

    public CRMService(IUserService userService)
    {
        _userService = userService;
    }

    public Task<double> GetUserMoney(UserEntity user)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.GetUserMoney(user.Cafe.CRMCredential, user.Username);
    }

    public Task<double> AddMoneyToUser(UserEntity user, double money)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.AddMoneyToUser(user.Cafe.CRMCredential, user.Username, money);
    }

    public Task<double> SetUserMoney(UserEntity user, double money)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.SetUserMoney(user.Cafe.CRMCredential, user.Username, money);
    }

    public Task<double> GetUserBonus(UserEntity user)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.GetUserBonus(user.Cafe.CRMCredential, user.Username);
    }

    public Task<double> AddBonusToUser(UserEntity user, double money)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.AddBonusToUser(user.Cafe.CRMCredential, user.Username, money);
    }

    public Task<double> SetUserBonus(UserEntity user, double money)
    {
        var crm = AbstractCRM.GetCRMByType(user.Cafe.CRMType);
        return crm.SetUserBonus(user.Cafe.CRMCredential, user.Username, money);
    }
}