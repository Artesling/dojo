﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.Module;

namespace Dojo.API.Services;

public class ModuleService : IModuleService
{
    private readonly IMapper _mapper;
    
    public ModuleService(IMapper mapper)
    {
        _mapper = mapper;
    }

    public ModuleEntity? GetModuleById(long id)
    {
        return Database.Get<ModuleEntity>().FirstOrDefault(x => x.Id == id);
    }

    public ModuleEntity? GetModuleByName(string name)
    {
        return Database.Get<ModuleEntity>().FirstOrDefault(x => x.Name == name);
    }

    public ModuleEntity CreateModule(string name)
    {
        var module = new ModuleEntity
        {
            Name = name,
            State = ModuleState.Disabled
        };

        Database.UpdateOrInsertAsync(module);

        return module;
    }

    public ModuleEntity ChangeModuleState(long moduleId, ModuleState newState)
    {
        var module = Database.Get<ModuleEntity>()
            .AsQueryable()
            .FirstOrDefault(x => x.Id == moduleId);

        module.State = newState;
        
        Database.UpdateOrInsertAsync(module);

        return module;
    }
}