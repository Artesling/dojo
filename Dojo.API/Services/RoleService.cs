﻿using Dojo.API.Entities;
using Dojo.API.Interfaces.Role;

namespace Dojo.API.Services;

public class RoleService : IRoleService
{
    public RoleEntity CreateNewRole(string name)
    {
        var role = new RoleEntity { Name = name };
        
        Database.UpdateOrInsert(role);
        
        return role;
    }
}