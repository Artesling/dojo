﻿using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.Log;

namespace Dojo.API.Services;

public class LogService : ILogService
{
    public LogService()
    {
    }

    public LogEntity CreateUserLog(LogType type, DateTime date, string message, ModuleEntity module, UserEntity user)
    {
        var newLog = new LogEntity
        {
            Type = type,
            Date = date,
            Message = message,
            Module = module,
            User = user
        };

        Database.Get<LogEntity>().Add(newLog);

        return newLog;
    }

    public LogEntity CreateModuleLog(LogType type, string message, ModuleEntity module)
    {
        var currentDateTime = DateTime.Now;
        
        var newLog = new LogEntity
        {
            Type = type,
            Date = currentDateTime,
            Message = message,
            Module = module
        };

        Database.Get<LogEntity>().Add(newLog);

        return newLog;
    }
}