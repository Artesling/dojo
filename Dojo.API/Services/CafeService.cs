﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.Cafe;
using Dojo.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.Services;

public class CafeService : ICafeService
{
    private readonly IMapper _mapper;
    
    public CafeService(IMapper mapper)
    {
        _mapper = mapper;
    }

    public async Task<List<CafeEntity>> GetAllCafesAsync()
    {
        var cafes = await Database.Get<CafeEntity>()
            .Include(x => x.Users)
            .ToListAsync();
        return cafes;
    }

    public async Task<long> GetNextGambleSettingByCafeIdAsync(long cafeId)
    {
        var cafe = await Database.Get<CafeEntity>()
            .Include(x => x.CafeSettings)
            .FirstOrDefaultAsync(x => x.Id == cafeId);

        return long.Parse(cafe.CafeSettings.FirstOrDefault(x => x.Id == (long) CafeSettings.NextGambleUse).Value);
    }

    public async Task<CafeEntity> CreateCafeAsync(CafeModel cafe)
    {
        var cafeEntity = _mapper.Map<CafeEntity>(cafe);
        cafeEntity.CafeSettings = new List<CafeSettingEntity>();

        await Database.UpdateOrInsertAsync(cafeEntity);
        return cafeEntity;
    }

    public async Task<CafeEntity> GetCafeByNameAsync(string name)
    {
        return await Database.Get<CafeEntity>()
            .Include(x => x.Social)
            .Include(x => x.CafeSettings)
            .FirstOrDefaultAsync(x => x.Name.ToLower() == name.ToLower());
    }

    public async Task<CafeEntity> GetCafeByDiscordIdAsync(ulong discordServerId)
    {
        return await Database.Get<CafeEntity>()
            .Include(x => x.Social)
            .Include(x => x.CafeSettings)
            .FirstOrDefaultAsync(x => x.Social.DiscordServerId == discordServerId);
    }
}