﻿using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.GlobalSetting;
using Microsoft.EntityFrameworkCore;

namespace Dojo.API.Services;

public class GlobalSettingService : IGlobalSettingService
{
    public string GetSettingValueSync(GlobalSettings settingId)
    {
        return Database.Get<GlobalSettingEntity>().FirstOrDefault(x => x.Id == (long) settingId).Value;
    }

    public long GetDefaultRoleIdSync()
    {
        var roleId = Database.Get<GlobalSettingEntity>().FirstOrDefault(x => x.Id == (long) GlobalSettings.DefaultRoleId);
        return long.Parse(roleId.Value);
    }

    public async Task<string> GetDiscordDevBotTokenAsync()
    {
        var botToken = await Database.Get<GlobalSettingEntity>()
            .FirstOrDefaultAsync(x => x.Id == (long)GlobalSettings.DiscordDevBotToken);
        return botToken.Value;
    }
    
    public async Task<string> GetDiscordProdBotTokenAsync()
    {
        var botToken = await Database.Get<GlobalSettingEntity>()
            .FirstOrDefaultAsync(x => x.Id == (long)GlobalSettings.DiscordProdBotToken);
        return botToken.Value;
    }
}