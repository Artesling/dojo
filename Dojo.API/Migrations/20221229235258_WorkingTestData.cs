﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dojo.API.Migrations
{
    /// <inheritdoc />
    public partial class WorkingTestData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("CafeCRMCredentials" , new []
            {
                "Id" , 
                "LicenseId", 
                "CafeId",
                "Login",
                "Password",
                "AuthToken"
            } , new []
            {
                "1", 
                "719522759",
                "72541",
                null,
                null,
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMTE4NTQ2YjZjM2U2OTIyN2ZkNWMyMjc5MWYxZDU0MjJjOGE2MzA5MmY1MjQ4MDg3ZDFjMDQ0OTU3OGFiYmE5Mjk5YjgzMzkyOTFjZThiMmMiLCJpYXQiOjE2NzIyNTkzOTAuNDY5MDU3LCJuYmYiOjE2NzIyNTkzOTAuNDY5MDYsImV4cCI6MTcwMzc5NTM5MC40Njc2OSwic3ViIjoiMzg0MTE3NTg0MyIsInNjb3BlcyI6W119.KbjmPzJVBF7APZKy5UingwXy0O5piP3Fo5EPGHyAGPGpKtVa1D9MQRG9JmWo6xs8SbTOZDosQZkN89OMKyaCy5GvCe0mpUwDdgRZ3tkB9Wozh9kuFIeZEWVxBtAhIF1Zp7bkchV0jV10G1a3OyQwTk7zLGeWu2OVns2qSiJrcvNbGiVdbhBhoNB5QC1_xIq5rKaldqzFj7VX10nS3CMFZd8nqLHLdyJTaPE5IezHD7NOXQztb2VG7kRe1nIDK5SynHkEMT7tT2IVMKtidH3tlVxeLNFX-AZ8_Wb16qyIec0lCw3hDRkAPFhcACyjHYd-4iLBcECha6az55mJ3PsxmOhQ5gniMIK0jDvyxjyVq9S-9UcjIOYGsRJOgqqGMxo9TUydIOJDKcpjwemg3RGnJMvOwFEyylyL_fj9jvftA6wRFiCqN-HbkgYmX2i5LzRFLaKTwzcX-bJDi4Wqemhg597_ibd9b-AAfhgrfzgxQNrzOSbVI4zgqNN_tZFja2z72psx2fVXaiNvWT5fjFAZkGmlxR--P_I6aptPjP_scGiM5zQ1xwdefRwQddqwPhJB7EBz20ZB252Ns2EOqNjeuQeh_HiY54LCn3papxSYlXmuQPIhjiF1Jj2Jp8DJ5KS_EYIhYgBJSeniE7ux15nvRlvPIe3VFxlhWWdgJIALlgI"
            });
            
            migrationBuilder.InsertData("CafeSocials" , new []
            {
                "Id" , 
                "Website", 
                "Phone",
                "Email",
                "DiscordServerId",
                "TelegramChatId"
            } , new []
            {
                "1", 
                null,
                null,
                null,
                "806596195266592840",
                null
            });
            
            migrationBuilder.InsertData("Cafes" , new []
            {
                "Id" , 
                "Name", 
                "Address",
                "CRMType",
                "SocialId",
                "CRMCredentialId"
            } , new []
            {
                "1", 
                "Dojo Пионерская",
                "asd",
                "1",
                "1",
                "1"
            });

            migrationBuilder.InsertData("CafeSettings" , new []
            {
                "Id" , 
                "Name", 
                "Value",
                "CafeEntityId"
            } , new []
            {
                "1", 
                "Next gamble use in minutes",
                "1440",
                "1"
            });

            migrationBuilder.InsertData("GlobalSettings" , new []
            {
                "Id" , 
                "Name", 
                "Value"
            } , new []
            {
                "1", 
                "Default role id",
                "1"
            });
            
            migrationBuilder.InsertData("PrizeTypes" , new []
            {
                "Id" , 
                "Name"
            } , new []
            {
                "1", 
                "Money"
            });
            
            migrationBuilder.InsertData("Prizes" , new []
            {
                "Id" , 
                "Name",
                "Quantity",
                "Chance",
                "TypeId"
            } , new []
            {
                "1", 
                "100rub",
                "100",
                "80",
                "1"
            });
            
            migrationBuilder.InsertData("Prizes" , new []
            {
                "Id" , 
                "Name",
                "Quantity",
                "Chance",
                "TypeId"
            } , new []
            {
                "2", 
                "500rub",
                "500",
                "20",
                "1"
            });
            
            migrationBuilder.InsertData("Roles" , new []
            {
                "Id" , 
                "Name"
            } , new []
            {
                "1", 
                "Guest"
            });

            migrationBuilder.InsertData("Users" , new []
            {
                "Id" , 
                "Username", 
                "Password",
                "CafeId",
                "RoleId"
            } , new []
            {
                "1",
                "vladuska",
                null,
                "1",
                "1"
            });
            
            migrationBuilder.InsertData("UserGambleInfo" , new []
            {
                "Id" , 
                "UserId", 
                "LastUseRoulette",
                "RouletteChance"
            } , new []
            {
                "1",
                "1",
                "0001-01-01 00:00:00.0000000",
                "50"
            });
            
            migrationBuilder.InsertData("UserSocials" , new []
            {
                "Id" , 
                "UserId", 
                "Phone",
                "Email",
                "DiscordId",
                "TelegramId",
                "VkontakteId"
            } , new []
            {
                "1",
                "1",
                null,
                null,
                null,
                null,
                null
            });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData("CafeCRMCredentials" , new []
            {
                "Id" , 
                "LicenseId", 
                "CafeId",
                "Login",
                "Password",
                "AuthToken"
            } , new []
            {
                "1", 
                "719522759",
                "72541",
                null,
                null,
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMTE4NTQ2YjZjM2U2OTIyN2ZkNWMyMjc5MWYxZDU0MjJjOGE2MzA5MmY1MjQ4MDg3ZDFjMDQ0OTU3OGFiYmE5Mjk5YjgzMzkyOTFjZThiMmMiLCJpYXQiOjE2NzIyNTkzOTAuNDY5MDU3LCJuYmYiOjE2NzIyNTkzOTAuNDY5MDYsImV4cCI6MTcwMzc5NTM5MC40Njc2OSwic3ViIjoiMzg0MTE3NTg0MyIsInNjb3BlcyI6W119.KbjmPzJVBF7APZKy5UingwXy0O5piP3Fo5EPGHyAGPGpKtVa1D9MQRG9JmWo6xs8SbTOZDosQZkN89OMKyaCy5GvCe0mpUwDdgRZ3tkB9Wozh9kuFIeZEWVxBtAhIF1Zp7bkchV0jV10G1a3OyQwTk7zLGeWu2OVns2qSiJrcvNbGiVdbhBhoNB5QC1_xIq5rKaldqzFj7VX10nS3CMFZd8nqLHLdyJTaPE5IezHD7NOXQztb2VG7kRe1nIDK5SynHkEMT7tT2IVMKtidH3tlVxeLNFX-AZ8_Wb16qyIec0lCw3hDRkAPFhcACyjHYd-4iLBcECha6az55mJ3PsxmOhQ5gniMIK0jDvyxjyVq9S-9UcjIOYGsRJOgqqGMxo9TUydIOJDKcpjwemg3RGnJMvOwFEyylyL_fj9jvftA6wRFiCqN-HbkgYmX2i5LzRFLaKTwzcX-bJDi4Wqemhg597_ibd9b-AAfhgrfzgxQNrzOSbVI4zgqNN_tZFja2z72psx2fVXaiNvWT5fjFAZkGmlxR--P_I6aptPjP_scGiM5zQ1xwdefRwQddqwPhJB7EBz20ZB252Ns2EOqNjeuQeh_HiY54LCn3papxSYlXmuQPIhjiF1Jj2Jp8DJ5KS_EYIhYgBJSeniE7ux15nvRlvPIe3VFxlhWWdgJIALlgI"
            });
            
            migrationBuilder.DeleteData("CafeSettings" , new []
            {
                "Id" , 
                "Name", 
                "Value",
                "CafeEntityId"
            } , new []
            {
                "1", 
                "Next gamble use in minutes",
                "1440",
                "1"
            });
            
            migrationBuilder.DeleteData("Cafes" , new []
            {
                "Id" , 
                "Name", 
                "Address",
                "CRMType",
                "SocialId",
                "CRMCredentialId"
            } , new []
            {
                "1", 
                "Dojo Пионерская",
                "asd",
                "1",
                "1",
                "1"
            });
            
            migrationBuilder.DeleteData("CafeSocials" , new []
            {
                "Id" , 
                "Website", 
                "Phone",
                "Email",
                "DiscordServerId",
                "TelegramChatId"
            } , new []
            {
                "1", 
                null,
                null,
                null,
                null,
                null
            });
            
            migrationBuilder.DeleteData("GlobalSettings" , new []
            {
                "Id" , 
                "Name", 
                "Value"
            } , new []
            {
                "1", 
                "Default role id",
                "1"
            });
            
            migrationBuilder.DeleteData("PrizeTypes" , new []
            {
                "Id" , 
                "Name"
            } , new []
            {
                "1", 
                "Money"
            });
            
            migrationBuilder.DeleteData("Prizes" , new []
            {
                "Id" , 
                "Name",
                "Quantity",
                "Chance",
                "TypeId"
            } , new []
            {
                "1", 
                "100rub",
                "100",
                "80",
                "1"
            });
            
            migrationBuilder.DeleteData("Prizes" , new []
            {
                "Id" , 
                "Name",
                "Quantity",
                "Chance",
                "TypeId"
            } , new []
            {
                "1", 
                "500rub",
                "500",
                "20",
                "1"
            });
            
            migrationBuilder.DeleteData("Roles" , new []
            {
                "Id" , 
                "Name"
            } , new []
            {
                "1", 
                "Guest"
            });
            
            migrationBuilder.DeleteData("UserGambleInfo" , new []
            {
                "Id" , 
                "UserId", 
                "LastUseRoulette",
                "RouletteChance"
            } , new []
            {
                "1",
                "1",
                "0001-01-01 00:00:00.0000000",
                "50"
            });
            
            migrationBuilder.DeleteData("Users" , new []
            {
                "Id" , 
                "Username", 
                "Password",
                "CafeId",
                "RoleId"
            } , new []
            {
                "1",
                "vladuska",
                null,
                "1",
                "1"
            });
            
            migrationBuilder.DeleteData("UserSocials" , new []
            {
                "Id" , 
                "UserId", 
                "Phone",
                "Email",
                "DiscordId",
                "TelegramId",
                "VkontakteId"
            } , new []
            {
                "1",
                "1",
                null,
                null,
                null,
                null,
                null
            });
        }
    }
}
