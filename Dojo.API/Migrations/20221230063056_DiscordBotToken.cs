﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dojo.API.Migrations
{
    /// <inheritdoc />
    public partial class DiscordBotToken : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("GlobalSettings", new[]
            {
                "Name",
                "Value"
            }, new[]
            {
                "Discord dev bot token",
                "MTAwODkyMjI4MTg3NzE4ODYzOA.GNJfNO.8fGdPCdWCeoyHr7fsHUIxERI76F4g-nef-mc9A"
            });
            
            migrationBuilder.InsertData("GlobalSettings", new[]
            {
                "Name",
                "Value"
            }, new[]
            {
                "Discord prod bot token",
                ""
            });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
