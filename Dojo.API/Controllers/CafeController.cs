﻿using AutoMapper;
using Dojo.API;
using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.Cafe;
using Dojo.API.Interfaces.CRM;
using Dojo.API.Interfaces.Log;
using Dojo.API.Interfaces.Module;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("[controller]/[action]")]
public class CafeController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ILogService _logService;
    private readonly IModuleService _moduleService;
    private readonly ICRMService _crmService;
    private readonly ICafeService _cafeService;
    
    public CafeController(IMapper mapper, ILogService logService, IModuleService moduleService, ICRMService crmService, ICafeService cafeService)
    {
        _mapper = mapper;
        _logService = logService;
        _moduleService = moduleService;
        _crmService = crmService;
        _cafeService = cafeService;
    }

    [HttpPost]
    public async Task<string> CreateCafe(string name,
        string address,
        CRMType crmType,
        string? website,
        string? phone,
        string? email,
        ulong? discordServerId,
        long? telegramChatId,
        long? licenseId,
        long? crmCafeId,
        string? crmLogin,
        string? crmPassword,
        string? crmAuthToken)
    {
        var social = new CafeSocialModel
        {
            Website = website,
            Phone = phone,
            Email = email,
            DiscordServerId = discordServerId,
            TelegramChatId = telegramChatId
        };

        var crmCredential = new CRMCredentialModel
        {
            LicenseId = licenseId,
            CafeId = crmCafeId,
            Login = crmLogin,
            Password = crmPassword,
            AuthToken = crmAuthToken
        };
        
        var cafe = new CafeModel
        {
            Name = name,
            Address = address,
            CRMType = crmType,
            Social = social,
            CRMCredential = crmCredential
        };

        await _cafeService.CreateCafeAsync(cafe);
        
        return "success";
    }

    [HttpGet]
    public async Task<List<CafeModel>> GetAllCafes()
    {
        var cafes = await _cafeService.GetAllCafesAsync();

        return _mapper.Map<List<CafeEntity>, List<CafeModel>>(cafes);
    }
}