﻿using System.Text;
using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Interfaces.User;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class UserController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IUserService _userService;

    public UserController(IMapper mapper, IUserService userService)
    {
        _mapper = mapper;
        _userService = userService;
    }

    [HttpGet]
    public async Task<List<UserModel>> GetAllUsers()
    {
        return await _userService.GetAllUsersAsync();
    }

    [HttpPost]
    public async Task<UserModel> CreateUser(string username, 
        long cafeId,
        string? password,
        string? phone,
        string? email,
        ulong? discordId,
        long? telegramId,
        long? vkontakteId)
    {
        var social = new UserSocialModel
        {
            Phone = phone,
            Email = email,
            DiscordId = discordId,
            TelegramId = telegramId,
            VkontakteId = vkontakteId
        };

        var user = new UserModel
        {
            Username = username,
            Password = password,
            CafeId = cafeId,
            Social = social
        };

        var newUser = await _userService.CreateUserAsync(user);
        
        return _mapper.Map<UserModel>(newUser);
    }

    [HttpPost]
    public UserModel AddRoleToUser(string username, string rolename)
    {
        return _mapper.Map<UserModel>(_userService.AddRoleToUserAsync(username, rolename));
    }

    [HttpGet]
    public RoleModel GetUserRoles(string username)
    {
        return _mapper.Map<RoleEntity, RoleModel>(_userService.GetUserRole(username));
    }

    [HttpPost]
    public async Task<ActionResult> UseGamble(string username, long cafeId)
    {
        var user = await _userService.GetUserByNameAndCafeIdAsync(username, cafeId);
        var minutesToNextGamble = await _userService.GetMinutesToNextGamble(user);

        if (minutesToNextGamble <= 0)
        {
            var prize = await _userService.UseGambleAsync(user);
            var res = new JsonResult(prize);
            res.StatusCode = 200;
            return res;
        }
        else
        {
            var res = new JsonResult("Рулетку нельзя сейчас использовать! Следующее использование через " + minutesToNextGamble + " минут!");
            res.StatusCode = 400;
            return res;
        }
    }
}