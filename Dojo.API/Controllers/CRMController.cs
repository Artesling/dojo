﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Enums;
using Dojo.API.Interfaces.CRM;
using Dojo.API.Interfaces.Log;
using Dojo.API.Interfaces.Module;
using Dojo.API.Interfaces.User;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class CRMController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ILogService _logService;
    private readonly IModuleService _moduleService;
    private readonly ICRMService _crmService;
    private readonly IUserService _userService;
    
    public CRMController(IMapper mapper, ILogService logService, IModuleService moduleService, ICRMService crmService, IUserService userService)
    {
        _mapper = mapper;
        _logService = logService;
        _moduleService = moduleService;
        _crmService = crmService;
        _userService = userService;
    }

    [HttpGet]
    public async Task<double> GetUserMoney(string username)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.GetUserMoney(user);
        return money;
    }
    [HttpGet]
    public async Task<double> GetUserBonus(string username)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.GetUserBonus(user);
        return money;
    }
    [HttpPost]
    public async Task<double> AddBonusToUser(string username, double sum)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.AddBonusToUser(user, sum);
        return money;
    }
    [HttpPost]
    public async Task<double> AddMoneyToUser(string username, double sum)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.AddMoneyToUser(user, sum);
        return money;
    }
    [HttpPost]
    public async Task<double> SetUserMoney(string username, double sum)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.SetUserMoney(user, sum);
        return money;
    }
    [HttpPost]
    public async Task<double> SetUserBonus(string username, double sum)
    {
        var user = Database.Get<UserEntity>()
            .Include(x => x.Cafe)
            .Include(x => x.Cafe.CRMCredential)
            .FirstOrDefault(x => x.Username == username);
        var money = await _crmService.SetUserBonus(user, sum);
        return money;
    }
}