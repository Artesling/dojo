﻿using AutoMapper;
using Dojo.API.Enums;
using Dojo.API.Interfaces.Log;
using Dojo.API.Interfaces.Module;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class ModuleController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ILogService _logService;
    private readonly IModuleService _moduleService;
    
    public ModuleController(IMapper mapper, ILogService logService, IModuleService moduleService)
    {
        _mapper = mapper;
        _logService = logService;
        _moduleService = moduleService;
    }
    
    [HttpPost]
    public ModuleModel CreateModule(string name)
    {
        var module = _moduleService.CreateModule(name);
        return _mapper.Map<ModuleModel>(module);
    }
}