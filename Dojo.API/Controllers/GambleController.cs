﻿using AutoMapper;
using Dojo.API.Entities;
using Dojo.API.Interfaces.Prize;
using Dojo.API.Interfaces.User;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class GambleController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IGambleService _gambleService;

    public GambleController(IMapper mapper, IGambleService gambleService)
    {
        _mapper = mapper;
        _gambleService = gambleService;
    }

    [HttpGet]
    public async Task<PrizeModel> GetRandomPrizeForUser(long userId)
    {
        return _mapper.Map<PrizeModel>(await _gambleService.GetRandomPrizeForUserAsync(userId));
    }

    [HttpPost]
    public async Task<PrizeModel> CreatePrize(string name, long quantity, double chance, string typeName)
    {
        var prizeType = await _gambleService.GetPrizeTypeByNameAsync(typeName);

        return _mapper.Map<PrizeModel>(await _gambleService.CreatePrizeAsync(name, quantity, chance, prizeType));
    }
    
    [HttpPost]
    public async Task<PrizeTypeModel> CreatePrizeType(string name)
    {
        return _mapper.Map<PrizeTypeModel>(await _gambleService.CreatePrizeTypeAsync(name));
    }
}