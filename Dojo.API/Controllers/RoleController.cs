﻿using AutoMapper;
using Dojo.API.Interfaces.Role;
using Dojo.API.Models;
using Dojo.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class RoleController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IRoleService _roleService;
    
    public RoleController(IMapper mapper, IRoleService roleService)
    {
        _mapper = mapper;
        _roleService = roleService;
    }

    [HttpPost]
    public RoleModel CreateRole(string name)
    {
        var role = _roleService.CreateNewRole(name);
        return _mapper.Map<RoleModel>(role);
    }
}