﻿using AutoMapper;
using Dojo.API.Enums;
using Dojo.API.Interfaces.CRM;
using Dojo.API.Interfaces.Log;
using Dojo.API.Interfaces.Module;
using Dojo.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dojo.API.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class LogController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ILogService _logService;
    private readonly IModuleService _moduleService;
    public LogController(IMapper mapper, ILogService logService, IModuleService moduleService)
    {
        _mapper = mapper;
        _logService = logService;
        _moduleService = moduleService;
    }
    
    [HttpPost]
    public async Task<LogModel> CreateLog(LogType type, string message, long moduleId)
    {
        var module = _moduleService.GetModuleById(moduleId);
        var log = _logService.CreateModuleLog(type, message, module);
        return _mapper.Map<LogModel>(log);
    }
}