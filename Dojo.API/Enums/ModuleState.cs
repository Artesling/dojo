﻿namespace Dojo.API.Enums;

public enum ModuleState
{
    Disabled = 0,
    Enabled = 1,
    Working = 2,
    Stopped = 3
}