﻿namespace Dojo.API.Enums;

public enum LogType
{
    Info = 0,
    Success = 1,
    Error = 2
}