﻿namespace Dojo.API.Enums;

public enum GlobalSettings
{
    DefaultRoleId = 1,
    DiscordDevBotToken = 2,
    DiscordProdBotToken = 3
}