﻿using Dojo.API.Interfaces.Cafe;
using Dojo.API.Interfaces.CRM;
using Dojo.API.Interfaces.Discord;
using Dojo.API.Interfaces.Log;
using Dojo.API.Interfaces.Module;
using Dojo.API.Interfaces.Prize;
using Dojo.API.Interfaces.Role;
using Dojo.API.Interfaces.GlobalSetting;
using Dojo.API.Interfaces.User;
using Dojo.API.Services;

namespace Dojo.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /*using (ApplicationContext db = new ApplicationContext())
            {
                UserEntity user1 = new UserEntity { Username = "test", TelegramId = 228};
                
                db.Users.Add(user1);
                db.SaveChanges();

                var users = db.Users.ToList();
                foreach (UserEntity u in users)
                {
                    Console.WriteLine($"{u.Id}.{u.Username} - {u.DiscordId} - {u.TelegramId}");
                }
            }*/

            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddControllers();
            
            builder.Services.AddTransient<IUserService, UserService>();
            builder.Services.AddTransient<IRoleService, RoleService>();
            builder.Services.AddTransient<ILogService, LogService>();
            builder.Services.AddTransient<IModuleService, ModuleService>();
            builder.Services.AddTransient<IGambleService, GambleService>();
            builder.Services.AddTransient<ICRMService, CRMService>();
            builder.Services.AddTransient<ICafeService, CafeService>();
            builder.Services.AddTransient<IGlobalSettingService, GlobalSettingService>();
            builder.Services.AddTransient<IDiscordService, DiscordService>();
            
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddAutoMapper(typeof(Program));

            var app = builder.Build();
            
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();
            app.MapControllers();

            app.Run();
        }
    }
}