﻿namespace Dojo.API.Exceptions.System;

public class CannotBeImplemented : Exception
{
    public CannotBeImplemented(string methodName) : base($"Method '{methodName}' cannot be implemented due to CRM restrictions!")
    {
    }
}