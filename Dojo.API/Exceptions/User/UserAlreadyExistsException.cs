﻿namespace Dojo.API.Exceptions.User;

public class UserAlreadyExistsException : Exception
{
    public UserAlreadyExistsException(string username) : base($"User with same username '{username}' already exists!")
    {
    }

    //discord
    public UserAlreadyExistsException(ulong discordUserId) : base($"Пользователь <@{discordUserId}> уже зарегистрирован!")
    {
    }
}