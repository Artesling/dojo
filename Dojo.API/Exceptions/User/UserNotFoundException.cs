﻿namespace Dojo.API.Exceptions.User;

public class UserNotFoundException : Exception
{
    public UserNotFoundException(string username) : base($"User with username '{username}' not found!")
    {
    }
    public UserNotFoundException(string username, long cafeId) : base($"User with username '{username}' and cafe id '{cafeId}' not found!")
    {
    }
}