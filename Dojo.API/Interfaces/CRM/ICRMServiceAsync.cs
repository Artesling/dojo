﻿using Dojo.API.Entities;

namespace Dojo.API.Interfaces.CRM;

public interface ICRMServiceAsync
{
    #region Money

    Task<double> GetUserMoney(UserEntity user);
    Task<double> AddMoneyToUser(UserEntity user, double money);
    Task<double> SetUserMoney(UserEntity user, double money);

    #endregion

    #region Bonus

    Task<double> GetUserBonus(UserEntity user);
    Task<double> AddBonusToUser(UserEntity user, double money);
    Task<double> SetUserBonus(UserEntity user, double money);

    #endregion
}