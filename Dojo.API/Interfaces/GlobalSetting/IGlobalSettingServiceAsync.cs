﻿using Dojo.API.Entities;

namespace Dojo.API.Interfaces.GlobalSetting;

public interface IGlobalSettingServiceAsync
{
    Task<string> GetDiscordDevBotTokenAsync();
    Task<string> GetDiscordProdBotTokenAsync();
}