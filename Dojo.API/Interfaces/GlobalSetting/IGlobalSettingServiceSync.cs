﻿using Dojo.API.Entities;
using Dojo.API.Enums;

namespace Dojo.API.Interfaces.GlobalSetting;

public interface IGlobalSettingServiceSync
{
    string GetSettingValueSync(GlobalSettings settingId);
    long GetDefaultRoleIdSync();
}