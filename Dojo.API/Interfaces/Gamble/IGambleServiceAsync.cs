﻿using Dojo.API.Controllers;
using Dojo.API.Entities;

namespace Dojo.API.Interfaces.Prize;

public interface IGambleServiceAsync
{
    Task<PrizeEntity> GetRandomPrizeForUserAsync(long userId);
    Task<List<PrizeEntity>> GetAllPrizesAsync();
    Task<PrizeEntity> CreatePrizeAsync(string name, long quantity, double chance, PrizeTypeEntity type);
    Task<UserGambleInfoEntity> GetUserGambleInfoEntityByIdAsync(long userId);
    Task<List<PrizeTypeEntity>> GetAllPrizeTypesAsync();
    Task<PrizeTypeEntity> GetPrizeTypeByIdAsync(long id);
    Task<PrizeTypeEntity> GetPrizeTypeByNameAsync(string name);
    Task<PrizeTypeEntity> CreatePrizeTypeAsync(string name);
}