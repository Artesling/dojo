﻿using Dojo.API.Entities;

namespace Dojo.API.Interfaces.Role;

public interface IRoleServiceSync
{
    RoleEntity CreateNewRole(string name);
}