﻿using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Interfaces.Cafe;

public interface ICafeServiceAsync
{
    Task<CafeEntity> CreateCafeAsync(CafeModel cafe);
    Task<List<CafeEntity>> GetAllCafesAsync();
    Task<CafeEntity> GetCafeByNameAsync(string name);
    Task<long> GetNextGambleSettingByCafeIdAsync(long cafeId);
    Task<CafeEntity> GetCafeByDiscordIdAsync(ulong discordServerId);
}