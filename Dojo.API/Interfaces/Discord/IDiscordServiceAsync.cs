﻿namespace Dojo.API.Interfaces.Discord;

public interface IDiscordServiceAsync
{
    Task<string> GetDiscordDevBotToken();
    Task<string> GetDiscordProdBotToken();
    Task<bool> RegisterUserAsync(ulong discordUserId, string username, ulong discordServerId);
}