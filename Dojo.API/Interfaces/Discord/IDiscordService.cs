﻿namespace Dojo.API.Interfaces.Discord;

public interface IDiscordService : IDiscordServiceAsync, IDiscordServiceSync
{
    
}