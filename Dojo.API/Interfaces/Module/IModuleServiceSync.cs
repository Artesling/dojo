﻿using Dojo.API.Entities;
using Dojo.API.Enums;

namespace Dojo.API.Interfaces.Module;

public interface IModuleServiceSync
{
    ModuleEntity? GetModuleById(long id);
    ModuleEntity? GetModuleByName(string name);
    ModuleEntity CreateModule(string name);
    ModuleEntity ChangeModuleState(long moduleId, ModuleState newState);
}