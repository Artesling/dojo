﻿using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Interfaces.User;

public interface IUserServiceSync
{
    RoleEntity GetUserRole(string username);
    Task<UserEntity?> GetUserByNameAndCafeIdAsync(string username, long cafeId);
    Task<PrizeModel> UseGambleAsync(UserEntity user);
}