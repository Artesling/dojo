﻿using Dojo.API.Entities;
using Dojo.API.Models;

namespace Dojo.API.Interfaces.User;

public interface IUserServiceAsync
{
    Task<long> GetMinutesToNextGamble(UserEntity user);
    Task<List<UserModel>> GetAllUsersAsync();
    Task<UserEntity> CreateUserAsync(UserModel user);
    Task<UserModel> AddRoleToUserAsync(string username, string roleName);
    Task<UserEntity?> GetUserByDiscordIdAndServerIdAsync(ulong discordUserId, ulong discordServerId);
    Task<UserEntity?> GetUserByDiscordIdAndCafeIdAsync(ulong discordUserId, long cafeId);
    Task<UserEntity> UpdateUserSocialAsync(UserSocialModel social, long userId);
    Task<UserEntity> GetUserByIdAsync(long userId);
}