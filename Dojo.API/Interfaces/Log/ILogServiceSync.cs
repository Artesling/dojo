﻿using Dojo.API.Entities;
using Dojo.API.Enums;

namespace Dojo.API.Interfaces.Log;

public interface ILogServiceSync
{
    LogEntity CreateUserLog(LogType type, DateTime date, string message, ModuleEntity module, UserEntity user);
    LogEntity CreateModuleLog(LogType type, string message, ModuleEntity module);
}